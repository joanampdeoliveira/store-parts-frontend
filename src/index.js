/* eslint-disable array-callback-return */
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter'
import configureStore from './store/configureStore'
import { addStorePart } from './actions/storeParts'

import './index.css'

const store = configureStore()

const fetchStoreParts = async () => {
  fetch('http://localhost:8081/store/parts')
    .then((response) => response.json())
    .then((results) => {
      results.map((part) => {
        store.dispatch(addStorePart(part))
      })
    })
}

fetchStoreParts()

const jxs = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
)

ReactDOM.render(jxs, document.getElementById('root'))
