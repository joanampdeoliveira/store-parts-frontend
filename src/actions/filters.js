export const setTextFilter = (text = '') => ({
  type: 'SET_TEXT_FILTER',
  text,
})

export const setPartTypeFilter = (partType = '') => ({
  type: 'SET_PART_TYPE_FILTER',
  partType,
})

export const sortByPrice = (priceSort = '') => ({
  type: 'SORT_BY_PRICE',
  priceSort,
})
