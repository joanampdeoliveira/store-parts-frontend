export const addStorePart = ({ name = '', type = '', price = '' } = {}) => ({
  type: 'ADD_STORE_PART',
  storePart: {
    name,
    type,
    price,
  },
})
