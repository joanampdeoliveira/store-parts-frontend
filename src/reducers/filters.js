const filtersReducerDefaultState = {
  text: '',
  partType: '',
  priceSort: '',
}

const filtersReducer = (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case 'SET_TEXT_FILTER':
      return {
        ...state,
        text: action.text,
      }
    case 'SET_PART_TYPE_FILTER':
      return {
        ...state,
        partType: action.partType,
      }
    case 'SORT_BY_PRICE':
      return {
        ...state,
        priceSort: action.priceSort,
      }
    default:
      return state
  }
}

export default filtersReducer
