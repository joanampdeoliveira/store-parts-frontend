const storePartsReducersDefaultState = []

const storePartsReducer = (state = storePartsReducersDefaultState, action) => {
  switch (action.type) {
    case 'ADD_STORE_PART':
      return [...state, action.storePart]
    default:
      return state
  }
}

export default storePartsReducer
