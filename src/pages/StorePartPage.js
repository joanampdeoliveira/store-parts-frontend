import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Card from '../components/Card'
import Loading from '../components/Loading'

const StorePartPage = ({ part }) => (
  <div>
    <a href="/">&#8592; Back to List</a>
    {part ? <Card item={part} /> : <Loading />}
  </div>
)

const mapStateToProps = (state, props) => ({
  part: state.storeParts.filter(
    (part) => part.name.replace(' ', '_') === props.match.params.storePart,
  )[0],
})

export default connect(mapStateToProps)(StorePartPage)

StorePartPage.propTypes = {
  part: PropTypes.object,
}
