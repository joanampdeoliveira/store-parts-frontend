import React from 'react'
import { connect } from 'react-redux'

import PropTypes from 'prop-types'

import getVisibleStoreParts from '../selectors/storeParts'

import List from '../components/List'
import StorePartsListFilters from '../components/StorePartsListFilters'
import Loading from '../components/Loading'

const StorePartsListPage = ({ storeParts }) => (
  <>
    {storeParts.length !== 0 ? (
      <>
        <StorePartsListFilters />
        <List items={storeParts} />
      </>
    ) : (
      <Loading />
    )}
  </>
)

const mapStateToProps = (state) => ({
  storeParts: getVisibleStoreParts(state.storeParts, state.filters),
})

export default connect(mapStateToProps)(StorePartsListPage)

StorePartsListPage.propTypes = {
  storeParts: PropTypes.array,
}
