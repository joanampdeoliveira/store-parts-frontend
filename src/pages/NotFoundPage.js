import React from 'react'

const NotFoundPage = () => (
  <div className="notFoundPageWrapper">
    <div className="notFoundWrapper">
      <img src="/gifs/404.gif" alt="404" />
    </div>
    <p>Page not found 404</p>
  </div>
)

export default NotFoundPage
