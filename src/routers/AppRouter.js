import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import StorePartsListPage from '../pages/StorePartsListPage'
import StorePartPage from '../pages/StorePartPage'
import NotFoundPage from '../pages/NotFoundPage'

const AppRouter = () => (
  <BrowserRouter>
    <h1 className="pageHeader">
      <a href="/">Store Parts</a>
    </h1>
    <Switch>
      <Route path="/" component={StorePartsListPage} exact />
      <Route path="/part/:storePart" component={StorePartPage} exact />
      <Route component={NotFoundPage} />
    </Switch>
  </BrowserRouter>
)

export default AppRouter
