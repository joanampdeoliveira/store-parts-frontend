import React from 'react'
import PropTypes from 'prop-types'

const Card = ({ item }) => (
  <div className="cardWrapper">
    <h2>{item.name}</h2>
    <span>{item.type}</span>
    <p>{item.price}</p>
  </div>
)

export default Card

Card.propTypes = {
  item: PropTypes.object,
}
