import React from 'react'

const Loading = () => (
  <div className="loadingWrapper">
    <img src="/gifs/loading.gif" alt="loading"></img>
  </div>
)

export default Loading
