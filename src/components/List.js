import React from 'react'
import PropTypes from 'prop-types'

import ListItem from './ListItem'

const List = ({ items }) => (
  <ul className="storePartsList">
    {items.map((item) => (
      <ListItem item={item} />
    ))}
  </ul>
)

export default List

List.propTypes = {
  items: PropTypes.object,
}
