import React from 'react'
import PropTypes from 'prop-types'

const ListItem = ({ item }) => (
  <li className="storePartsListItem">
    <a href={`part/${item.name.replace(' ', '_')}`}>
      <span>{item.name}</span>
      <span>{item.type}</span>
      <span>{item.price}</span>
    </a>
  </li>
)

export default ListItem

ListItem.propTypes = {
  item: PropTypes.object,
}
