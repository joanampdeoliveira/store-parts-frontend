import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  setTextFilter,
  setPartTypeFilter,
  sortByPrice,
} from '../actions/filters'

const StorePartsListFilters = ({ dispatch, filters }) => {
  const [partTypes, setPartTypes] = useState([])

  const handleTextFilterOnChange = (e) => {
    dispatch(setTextFilter(e.target.value))
  }
  const handleTypeFilterOnChange = (e) => {
    dispatch(setPartTypeFilter(e.target.value))
  }
  const handlePriceFilterOnChange = (e) => {
    dispatch(sortByPrice(e.target.value))
  }

  useEffect(() => {
    fetch('http://localhost:8081/store/part-types')
      .then((response) => response.json())
      .then((results) => {
        setPartTypes(results)
      })
  }, [])

  return (
    <div className="listFilterWrapper">
      <input
        type="text"
        value={filters.text}
        onChange={handleTextFilterOnChange}
      />
      <select value={filters.partType} onChange={handleTypeFilterOnChange}>
        <option value="">select type</option>
        {partTypes.map((type) => (
          <option value={type}>{type}</option>
        ))}
      </select>
      <select value={filters.sortByPrice} onChange={handlePriceFilterOnChange}>
        <option value="">Order by price</option>
        <option value="asc">Price Ascendent</option>
        <option value="desc">Price Descendent</option>
      </select>
    </div>
  )
}

const mapStateToProps = (state) => ({
  filters: state.filters,
})

export default connect(mapStateToProps)(StorePartsListFilters)

StorePartsListFilters.propTypes = {
  dispatch: PropTypes.func,
  filters: PropTypes.array,
}
