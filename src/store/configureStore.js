/* eslint-disable import/no-anonymous-default-export */
import { createStore, combineReducers } from 'redux'
import filtersReducer from '../reducers/filters'
import storePartsReducer from '../reducers/storeParts'

export default () => {
  const store = createStore(
    combineReducers({
      storeParts: storePartsReducer,
      filters: filtersReducer,
    }),
  )
  return store
}
