/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
const getVisibleStoreParts = (storeParts, { text, partType, priceSort }) =>
  storeParts
    .filter((part) => {
      const partTypeMatch =
        partType === '' || part.type.toLowerCase() === partType.toLowerCase()
      const textMatch = part.name.toLowerCase().includes(text.toLowerCase())

      return partTypeMatch && textMatch
    })
    .sort((a, b) => {
      if (priceSort === 'asc') {
        return parseFloat(a.price) > parseFloat(b.price) ? 1 : -1
      }
      if (priceSort === 'desc') {
        return parseFloat(a.price) < parseFloat(b.price) ? 1 : -1
      }
    })

export default getVisibleStoreParts
